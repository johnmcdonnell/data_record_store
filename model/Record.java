package model;


/**
 * @author Delwyn Monaghan and John McDonnell. Course: Computing Level 8. Group:
 *         2. Module: AOOD CA2. Due Date: 1st May 2015.
 */
public class Record
{
	/*
	 * Variables
	 */
	private String title;
	private String description;
	private int numSeasons;
	private int launchDate;
	private double averageRunTime;
	private float rating;
	private boolean netflix;

	/*
	 * Constructor 
	 */
	public Record(String title, String description, int numSeasons,
			int launchDate, double averageRunTime, float rating, boolean netflix)
	{
		
		this.title = title;
		this.description = description;
		this.numSeasons = numSeasons;
		this.launchDate = launchDate;
		this.averageRunTime = averageRunTime;
		this.rating = rating;
		this.netflix = netflix;
	}
/*
 * Getters and Setters
 */
	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public int getNumSeasons()
	{
		return numSeasons;
	}

	public void setNumSeasons(int numSeasons)
	{
		this.numSeasons = numSeasons;
	}

	public int getLaunchDate()
	{
		return launchDate;
	}

	public void setLaunchDate(int launchDate)
	{
		this.launchDate = launchDate;
	}

	public double getAverageRunTime()
	{
		return averageRunTime;
	}

	public void setAverageRunTime(double averageRunTime)
	{
		this.averageRunTime = averageRunTime;
	}

	public float getRating()
	{
		return rating;
	}

	public void setRating(float rating)
	{
		this.rating = rating;
	}

	public boolean getNetflix()
	{
		return netflix;
	}

	public void setNetflix(boolean netflix)
	{
		this.netflix = netflix;
	}

	/*
	 * toString
	 */
	public String toString()
	{
		return "Tv Show Database" + "\n------------------------ "
				+ "\nTitle = " + title + "\nDescription = " + description
				+ "\nNumber of Seasons = " + numSeasons + "\nLaunch Date = "
				+ launchDate + "\nAverage RunTime = " + averageRunTime
				+ "\nRating = " + rating + "\nNetflix = " + netflix;
	}

}
