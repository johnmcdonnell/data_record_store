package control;

public class StringUtility 
{
	public static final int Pad_Length = 16;
	public static final char Pad_Char = '*';
	
	
	//Given John, '%', 8 we get John%%%%
	public static String pad(String data)
	{
		if(data == null)
			return null;
		
		int paddingRequired = Pad_Length - data.length();
		
		if(paddingRequired < 0)
			return null; //John12345
		else if(paddingRequired == 0)
			return data; //John1234
		
		StringBuilder strBuilder = new StringBuilder(data);
		
		for(int i = 0; i < paddingRequired; i++)
		{
			strBuilder.append(Pad_Char);
		}
		
		return strBuilder.toString();
	}
	
	
	public static String unpad(String data) //John1234
	{
		if(data == null)
			return null;
		
		int indexPadChar = data.indexOf(Pad_Char);
		
		//e.g. "John1234"
		if((indexPadChar == -1) && (data.length() == Pad_Length))
			return data;
		// i.e. < length and no pad char e.g. John1
		else if(indexPadChar == -1) 
			return null;
		
		return data.substring(0, indexPadChar);
	}
}



















