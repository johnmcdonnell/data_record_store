package control;

/*
 * Imports
 */
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;

import model.Record;

/**
 * @author Delwyn Monaghan and John McDonnell. Course: Computing Level 8. Group:
 *         2. Module: AOOD CA2. Due Date: 1st May 2015.
 */

public class FileUtility
{

/*
 * Declaring variables.
 * Creating instance of FileHandler.
 * Setting isOpen to false
 * Setting recordLength
 */
	private File file;
	private FileHandler fileHand = new FileHandler();
	private RandomAccessFile rFile;
	private StringUtility strut;
	private boolean isOpen = false;
	private int count;
	private final int recordLength = 58;
	private HashMap<String, Integer> hMap;

	/*
	 * Create File Method.
	 * Creates a file in FileHandler
	 * Returns boolean to say if file was created or not.
	 */
	public boolean createFile(String name)
	{
		boolean pass = fileHand.createFile(name);
		return pass;
	}
	/*
	 * Open File Method.
	 * Opens a file in FileHandler
	 * Returns a random access file.
	 * Gets the HashMap that was created in FileHandler which is unique to that file.
	 * Returns NULL if no file is created.
	 */
	public RandomAccessFile openFile(String name)
	{
		this.rFile = this.fileHand.openFile(name);
		if (rFile != null)
		{
			this.isOpen = true;
			this.hMap = this.fileHand.getHashMap();
			this.count = hMap.size();
			return this.rFile;
		}
		return null;
	}
	/*
	 * Delete File Method.
	 * Deletes a file in FileHandler
	 * Returns a boolean to say if file was deleted or not.
	 */
	public boolean deleteFile(String name)
	{
		boolean pass = fileHand.deleteFile(name);
		return pass;
	}
	/*
	 * Add Record Method.
	 * Takes Record. Checks if file is open. If that HashMap contains this record(Key) it returns false.
	 * Skips bytes need to reach end of file.
	 * Writes that record to the file.
	 * Update HashMap with that Key as title and position of record in the file.
	 * Increase count.
	 */
	public boolean add(Record record) throws IOException
	{
		if (isOpen)
		{
			if (!this.hMap.containsKey(record.getTitle()))
			{
				rFile.seek(0);
				int skip = (int) rFile.length();
				rFile.skipBytes(skip);

				rFile.writeBoolean(false);
				rFile.writeUTF(strut.pad(record.getTitle()));
				rFile.writeUTF(strut.pad(record.getDescription()));
				rFile.writeInt(record.getNumSeasons());
				rFile.writeInt(record.getLaunchDate());
				rFile.writeDouble(record.getAverageRunTime());
				rFile.writeFloat(record.getRating());
				rFile.writeBoolean(record.getNetflix());

				hMap.put(record.getTitle(), recordLength * count);
				count++;
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	/*
	 * Close File Method.
	 * If a random access file is open, then close it. Flag is set to false.
	 */
	public void closeFile() throws IOException
	{
		if (rFile != null)
		{
			rFile.close();
			this.isOpen = false;
		}
	}

	/*
	 * Search Record Method.
	 * Takes String Key. Checks if file is open. If that HashMap contains this record(Key) it returns NULL.
	 * Skips bytes need to reach the Key(Record).
	 * Checks record is not deleted.
	 * Reads that record from the file into record.
	 * Returns the record.
	 */
	public Record search(String key) throws IOException
	{
		Record r = null;
		if (isOpen)
		{
			rFile.seek(0);
			if (hMap.containsKey(key))
			{
				rFile.skipBytes(hMap.get(key));

				boolean isDeleted = rFile.readBoolean();
				if (isDeleted == false)
				{
					String title = strut.unpad(rFile.readUTF());
					String desc = strut.unpad(rFile.readUTF());
					int numSeasons = rFile.readInt();
					int launchDate = rFile.readInt();
					double runTime = rFile.readDouble();
					float rating = rFile.readFloat();
					boolean netflix = rFile.readBoolean();

					r = new Record(title, desc, numSeasons, launchDate,
							runTime, rating, netflix);
					return r;
				}
			}
		}
		return r;
	}

	/*
	 * Edit Record Method.
	 * Takes String Key and Record.
	 * Uses search method to find if record exists needs to be edited. If not return false.
	 * Keeps previous record key.
	 * Checks if file is open. If that HashMap contains this record(Key) it returns NULL.
	 * Skips bytes to previous records position.
	 * Checks record is not deleted.
	 * Writes that new record in. Removes previous record from the HashMap.
	 * Inserts new key with previous position into HashMap.
	 */
	public boolean edit(String key, Record record) throws IOException
	{
		Record r = search(key);
		if (r == null)
			return false;

		String prevName = key;
		if (isOpen)
		{
			rFile.seek(0);
			int skip = hMap.get(prevName);
			rFile.skipBytes(skip);

			boolean isDeleted = rFile.readBoolean();
			if (isDeleted == false)
			{
				rFile.seek(0);
				rFile.skipBytes(skip);

				rFile.writeBoolean(false);
				rFile.writeUTF(strut.pad(record.getTitle()));
				rFile.writeUTF(strut.pad(record.getDescription()));
				rFile.writeInt(record.getNumSeasons());
				rFile.writeInt(record.getLaunchDate());

				rFile.writeDouble(record.getAverageRunTime());
				rFile.writeFloat(record.getRating());
				rFile.writeBoolean(record.getNetflix());
			}
			hMap.remove(prevName);
			hMap.put(record.getTitle(), skip);
			return true;
		}
		return false;
	}

	/*
	 * Search By Regex Method.
	 * Takes String Regex. Checks if file is open. Creates an ArrayList, initialize boolean as false.
	 * Uses enhance for loop to loop through records in the file.
	 * Checks if each record is deleted.
	 * Reads record from the file.
	 * If record title matches regex, store record in the ArrayList.
	 * Set boolean as true.
	 * If boolean is true, return ArrayList, else return NULL.
	 */
	public ArrayList<Record> searchByRegex(String strRegex) throws IOException
	{
		if (isOpen)
		{
			boolean pass = false;
			ArrayList<Record> regexList = new ArrayList<Record>();
			for (String str : hMap.keySet())
			{
				rFile.seek(0);
				rFile.skipBytes(hMap.get(str));

				boolean isDeleted = rFile.readBoolean();
				if (isDeleted == false)
				{
					Record r;
					String title = strut.unpad(rFile.readUTF());
					if (RegexUtility.matches(title, strRegex) == true)
					{
						String desc = strut.unpad(rFile.readUTF());
						int numSeasons = rFile.readInt();
						int launchDate = rFile.readInt();
						double runTime = rFile.readDouble();
						float rating = rFile.readFloat();
						boolean netflix = rFile.readBoolean();

						r = new Record(title, desc, numSeasons, launchDate,
								runTime, rating, netflix);
						regexList.add(r);
						pass = true;
					}
				}
			}
			if (pass)
			{
				return regexList;
			}
			else
			{
				return null;
			}
		}
		return null;
	}

	/*
	 * Search By Range Method.
	 * Takes two integers. Checks if integers have been passed in correctly.
	 * Checks if file is open. Creates an ArrayList, initialize boolean as false.
	 * Uses enhance for loop to loop through records in the file.
	 * Checks if each record is deleted.
	 * Reads record from the file.
	 * If record launch date matches input range, store record in the ArrayList.
	 * Set boolean as true.
	 * If boolean is true, return ArrayList, else return NULL.
	 */
	public ArrayList<Record> searchByRange(int range1, int range2)
			throws IOException
	{
		if (range1 < range2)
		{
			if (isOpen)
			{
				boolean pass = false;
				ArrayList<Record> rangeList = new ArrayList<Record>();
				for (String str : hMap.keySet())
				{
					rFile.seek(0);
					rFile.skipBytes(hMap.get(str));

					boolean isDeleted = rFile.readBoolean();
					if (isDeleted == false)
					{
						Record r;
						String title = strut.unpad(rFile.readUTF());
						String desc = strut.unpad(rFile.readUTF());
						int numSeasons = rFile.readInt();
						int launchDate = rFile.readInt();
						if (launchDate >= range1 && launchDate <= range2)
						{
							double runTime = rFile.readDouble();
							float rating = rFile.readFloat();
							boolean netflix = rFile.readBoolean();

							r = new Record(title, desc, numSeasons, launchDate,
									runTime, rating, netflix);

							rangeList.add(r);
							pass = true;
						}
					}
				}
				if (pass)
				{
					return rangeList;
				}
				else
				{
					return null;
				}
			}
		}
		return null;
	}

	/*
	 * Delete Record Method.
	 * Takes String Key. Checks if file is open.
	 * Checks if HashMap contains Key. Checks if record is deleted. 
	 * If not set flag to true so it is flagged for deletion when the file closes. This record cannot be found again.
	 * Minus the count. 
	 * Return true if successful, else false.
	 */
	public boolean delete(String key) throws IOException
	{
		if (isOpen)
		{
			rFile.seek(0);
			if (hMap.containsKey(key))
			{
				int skip = hMap.get(key);
				rFile.skipBytes(skip);

				boolean isDeleted = rFile.readBoolean();
				if (!isDeleted)
				{
					rFile.seek(0);
					rFile.skipBytes(skip);
					rFile.writeBoolean(true);

					count--;
					return true;

				}
				return true;
			}
			return false;
		}
		return false;
	}
	/*
	 * Rewrite Method.
	 * Creates ArrayList of type record. Checks if file is open.
	 * Checks if HashMap contains Key. Checks if record is deleted. 
	 * Enhance for loop through every record.
	 * Checks if record id deleted.
	 * Reads record from the file. Check if flagged for deletion, if not then add record to the ArrayList.
	 * Close file, use delete all record method which recreates the file with the same name, but no records.
	 * Open the file, loop through the ArrayList and add records to the new file.
	 * Return true if successful, else false.
	 */
	public boolean rewriteData() throws IOException
	{
		ArrayList<Record> recordArray = new ArrayList<Record>();
		if (isOpen)
		{
			for (String str : hMap.keySet())
			{
				rFile.seek(0);
				rFile.skipBytes(hMap.get(str));

				boolean isDeleted = rFile.readBoolean();
				if (isDeleted == false)
				{
					Record r;
					String title = strut.unpad(rFile.readUTF());
					String desc = strut.unpad(rFile.readUTF());
					int numSeasons = rFile.readInt();
					int launchDate = rFile.readInt();
					double runTime = rFile.readDouble();
					float rating = rFile.readFloat();
					boolean netflix = rFile.readBoolean();

					r = new Record(title, desc, numSeasons, launchDate,
							runTime, rating, netflix);
					recordArray.add(r);
				}
			}
			closeFile();
			count = 0;
			deleteAll(fileHand.getName());
			openFile(fileHand.getName());
			for (int i = 0; i < recordArray.size(); i++)
			{
				add(recordArray.get(i));
			}
			return true;
		}

		return false;

	}

	/*
	 * Delete All Method.
	 * Takes String name. Creates a file.
	 * Sets count to zero.
	 */
	public void deleteAll(String name)
	{
		deleteFile(name);

		createFile(name);
		count = 0;
	}

	/*
	 * Show All Method.
	 * Checks if file is open. if count is greater than zero.
	 * Enhance for loop through every record.
	 * Check if deleted, if not then print out the records.
	 * Return true if successful, else false.
	 */
	public boolean showAll() throws IOException
	{
		if (isOpen)
		{
			if (count > 0)
			{
				for (String str : hMap.keySet())
				{
					rFile.seek(0);
					rFile.skipBytes(hMap.get(str));

					boolean isDeleted = rFile.readBoolean();
					if (isDeleted == false)
					{
						Record r;
						String title = strut.unpad(rFile.readUTF());
						String desc = strut.unpad(rFile.readUTF());
						int numSeasons = rFile.readInt();
						int launchDate = rFile.readInt();
						double runTime = rFile.readDouble();
						float rating = rFile.readFloat();
						boolean netflix = rFile.readBoolean();

						r = new Record(title, desc, numSeasons, launchDate,
								runTime, rating, netflix);

						System.out.println(r);
					}

				}
				return true;
			}
		}
		return false;
	}

	/*
	 * Count Method.
	 * Checks if file is open.
	 * Prints out the number of records.
	 */
	public void count()
	{
		if (isOpen)
		{
			System.out.println("Number of records is" + count);
		}
	}
/*
 * Getters and Setters
 */
	public File getFile()
	{
		return file;
	}

	public void setFile(File file)
	{
		this.file = file;
	}

	public FileHandler getFileHand()
	{
		return fileHand;
	}

	public void setFileHand(FileHandler fileHand)
	{
		this.fileHand = fileHand;
	}

	public RandomAccessFile getrFile()
	{
		return rFile;
	}

	public void setrFile(RandomAccessFile rFile)
	{
		this.rFile = rFile;
	}
}
