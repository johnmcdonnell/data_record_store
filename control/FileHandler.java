package control;

/*
 * Imports
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;

/**
 * @author Delwyn Monaghan and John McDonnell. Course: Computing Level 8. Group:
 *         2. Module: AOOD CA2. Due Date: 1st May 2015.
 */

public class FileHandler
{. 
	/*
	 * Declaring variables Setting a path for the file. Setting the regex for
	 * file name.
	 */
	private String path = "C:/temp/";
	private String name;
	private HashMap<String, Integer> hMap;
	private String strRegex = "[a-zA-Z0-9]{1,}.{0,}";
	private RandomAccessFile rFile;

	/*
	 * Create File Method Check String name matches the regex. Creates a file
	 * with desired path name and format. Check if that file exists. If not
	 * create new file, create new HashMap. Return true, else print error and
	 * return false.
	 */
	public boolean createFile(String name)
	{
		if (RegexUtility.matches(name, strRegex))
		{
			this.name = name;
			File file = new File(path + name + ".txt");

			if (!file.exists())
			{
				try
				{
					file.createNewFile();
					this.hMap = new HashMap<String, Integer>();
					return true;
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("Error - File already exists!");
				return false;
			}
		}
		return false;
	}

	/*
	 * Open File Method. Takes in String name. Creates a file with desired path
	 * name and format. Check if that file exists. If not create a random access
	 * file, that can we written to. Return random access file, else print error
	 * and return NULL.
	 */
	public RandomAccessFile openFile(String name)
	{
		File file = new File(path + name + ".txt");
		this.name = name;

		if (file.exists())
		{
			try
			{
				this.rFile = new RandomAccessFile(file, "rw");
				return rFile;
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	/*
	 * Delete File Method. Takes in String name. Creates a file with desired
	 * path name and format. Check if that file exists. If it does delete the
	 * file return true. else print error and return false.
	 */

	public boolean deleteFile(String name)
	{
		File file = new File(path + name + ".txt");
		if (file.exists())
		{
			file.delete();
			return true;
		}
		else
		{
			System.out.println("Error - File does not exists!");
			return false;
		}

	}

	/*
	 * HashMap Key String and Value Int. Creates a file with desired path name
	 * and format. Check if that file exists and if it can be written too.
	 * Return HashMap, else return NULL.
	 */
	public HashMap<String, Integer> getHashMap()
	{
		File file = new File(path + name + ".txt");

		if (file.exists() && file.canWrite())
		{
			return hMap;
		}
		return null;
	}

	/*
	 * Getters and Setters.
	 */
	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public RandomAccessFile getrFile()
	{
		return rFile;
	}

	public void setrFile(RandomAccessFile rFile)
	{
		this.rFile = rFile;
	}
}
