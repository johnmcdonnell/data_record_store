package control;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;


public class InputValidationUtility 
{
	public static String getString(Scanner kb, String strPrompt)
	{
		BufferedReader buffIn
		= new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print(strPrompt);
		String value = null;
		try 
		{
			value = buffIn.readLine();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return value;		
	}
	
	public static byte getByte(Scanner kb, 
			String strPrompt, byte lo, byte hi)
	{
		byte value = 0;
		
		do
		{
			value = getByte(kb, strPrompt);
			
		}while((value < lo) || (value > hi));
			
		return value;
	}
	
	public static byte getByte(Scanner kb, String strPrompt)
	{
		byte value = -1;
		boolean isValid = false;
		do
		{
			//ask user for value
			System.out.println(strPrompt);
			
			if(kb.hasNextByte() == true) //hasNextByte() checks but doesnt remove
			{
				//store value
				value = kb.nextByte();
				isValid = true;
			}
			else
			{
				System.out.println("Invalid!");
				kb.next(); //removes bad input from buffer
			}
		}while(isValid == false);
		
		return value;
	}
	
	public static double getDouble(Scanner kb, 
			String strPrompt, double lo, double hi)
	{
		double value = 0;
		
		do
		{
			value = getDouble(kb, strPrompt);
			
		}while((value < lo) || (value > hi));
			
		return value;
	}
	
	public static double getDouble(Scanner kb, String strPrompt)
	{
		double value = -1;
		boolean isValid = false;
		do
		{
			//ask user for value
			System.out.println(strPrompt);
			
			if(kb.hasNextDouble() == true) //hasNextDouble() checks but doesnt remove
			{
				//store value
				value = kb.nextDouble();
				isValid = true;
			}
			else
			{
				System.out.println("Invalid!");
				kb.next(); //removes bad input from buffer
			}
		}while(isValid == false);
		
		return value;
	}
	
	public static int getInt(Scanner kb, String strPrompt)
	{
		int value = -1;
		boolean isValid = false;
		do
		{
			//ask user for value
			System.out.println(strPrompt);
			
			if(kb.hasNextInt() == true) //hasNextInt() checks but doesnt remove
			{
				//store value
				value = kb.nextInt();
				isValid = true;
			}
			else
			{
				System.out.println("Invalid!");
				kb.next(); //removes bad input from buffer
			}
		}while(isValid == false);
		
		return value;
	}
		
	public static int getInt(Scanner kb, 
			String strPrompt, int lo, int hi)
	{
		int value = 0;
		
		do
		{
			value = getInt(kb, strPrompt);
			
		}while((value < lo) || (value > hi));
			
		return value;
	}

	 public static int menuValidation(int min, int max, String prompt, String errorPrompt)
	    {
	        Scanner keyboard = new Scanner(System.in);
	        int value;

	        while (!keyboard.hasNextInt())
	        {
	            System.out.print("Not an integer value - please re-enter.");
	            keyboard.nextLine();
	        }
	        value = keyboard.nextInt();

	        boolean notInRange = value < min || value > max;
	        while (notInRange)
	        {
	            System.out.println(errorPrompt + "[" + min + " : " + max + "]");
	            keyboard.nextLine();
	            System.out.print(prompt);

	            value = keyboard.nextInt();

	            notInRange = value < min || value > max;
	        }
	        return value;
	    }
}









