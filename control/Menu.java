package control;

/*
 * Imports.
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import model.Record;

/**
 * @author Delwyn Monaghan and John McDonnell. Course: Computing Level 8. Group:
 *         2. Module: AOOD CA2. Due Date: 1st May 2015.
 */
public class Menu
{
	/*
	 * Creating instance of FileUtility Declaring scanner
	 */
	FileUtility fileUtil = new FileUtility();
	private Scanner kb = new Scanner(System.in);

	/*
	 * Main Menu. Displays opening options for application. Checking user
	 * validation.
	 */
	private int getMainMenu()
	{
		System.out.println("\n\t\t ------ Main Menu ------ \n");
		System.out
				.println("Please choose one of the following options. *Note: File must be created before it can be opened. \n\n");
		System.out.println("1. Create File - Creates a new database file by specifying a file name and path");
		System.out.println("2. Open File - Opens an existing database by specifying a valid file name and path");
		System.out.println("3. Delete File - Deletes an existing database by specifying a valid file name and path");
		System.out.println("4. Close File - Closes an existing database by specifying a valid file name and path");
		System.out.println("5. Exit - Exits the application");
		int userChoice = InputValidationUtility.getInt(kb,
				"\nPlease enter your choice: ", 1, 5);

		return userChoice;
	}

	/*
	 * Main Menu Options. Loops until 5 is entered.
	 */
	public void displayMenu() throws IOException
	{
		int userChoice = getMainMenu();

		while (userChoice != 5)
		{
			if (userChoice == 1)
			{
				createFile();
			}
			else if (userChoice == 2)
			{
				openFile();
			}
			else if (userChoice == 3)
			{
				deleteFile();
			}
			else if (userChoice == 4)
			{
				closeFile();
			}
			else if (userChoice == 5)
			{
				System.out.println("\nSystem is now shutting down");
				System.exit(0);
			}
			else
			{
				System.out.println("Input not valid please try again");
			}

			userChoice = kb.nextInt();

		}
	}

	/*
	 * CreateFile Method. Prompts user for input. Passes nameOfFile into
	 * fileUtil.createFile. Displays Main Menu again.
	 */
	private void createFile() throws IOException
	{
		String strPrompt = "\nPlease enter the file name here: ";
		String nameOfFile = InputValidationUtility
				.getString(this.kb, strPrompt);
		fileUtil.createFile(nameOfFile);
		displayMenu();
	}

	/*
	 * OpenFile Method. Prompts user for input. Passes openFile into
	 * fileUtil.openFile. Displays amend record data menu again.
	 */
	private void openFile() throws IOException
	{
		String strPrompt = "\nPlease enter the file name you wish to open: ";
		String openFile = InputValidationUtility.getString(this.kb, strPrompt);
		fileUtil.openFile(openFile);
		ammendRecordData();
	}

	/*
	 * CloseFile Method. Closes the file that's currently open. Displays Main
	 * Menu again.
	 */
	private void closeFile() throws IOException
	{
		fileUtil.closeFile();
		displayMenu();
	}

	/*
	 * DeleteFile Method. Prompts user for input. Passes deleteFile into
	 * fileUtil.deleteFile. Displays Main Menu again.
	 */
	private void deleteFile() throws IOException
	{
		String strPrompt = "\nPlease enter the file name you wish to delete: ";
		String deleteFile = InputValidationUtility
				.getString(this.kb, strPrompt);
		fileUtil.deleteFile(deleteFile);
		displayMenu();
	}

	/*
	 * Data Menu. Displays file amending options for application. Checking user
	 * validation.
	 */
	private int getDataMenu()
	{
		System.out.println("\n\t\t ------ Data Menu ------ \n");
		System.out.println("Please choose one of the following options. \n\n");
		System.out.println("1. Add - Adds a new record to the database");
		System.out.println("2. Edit - Edits any of the fields of a given record in the database");
		System.out.println("3. Search By Regex - Displays the details of all records matching the regular expression specified");
		System.out.println("4. Search By Range - Displays the details of all records corresponding to a user-specifed range");
		System.out.println("5. Delete - Deletes a user-specified record from the database");
		System.out.println("6. Delete All - Deletes all records from the database");
		System.out.println("7. Show All - Display all records in the database");
		System.out.println("8. Count - Displays the total number of records in the database");
		System.out.println("9. Exit - Exits the application");

		int userChoice = InputValidationUtility.getInt(kb,
				"\nPlease enter your choice: ", 1, 9);

		return userChoice;
	}

	/*
	 * Data Menu Options. Loops until 10 is entered.
	 */
	public void ammendRecordData() throws IOException
	{
		int userChoice = getDataMenu();

		while (userChoice != 10)
		{
			if (userChoice == 1)
			{
				addMenu();
			}
			else if (userChoice == 2)
			{
				editMenu();
			}
			else if (userChoice == 3)
			{
				searchByRegexMenu();
			}
			else if (userChoice == 4)
			{
				searchByRangeMenu();
			}
			else if (userChoice == 5)
			{
				deleteRecordMenu();
			}
			else if (userChoice == 6)
			{
				deleteAllMenu();
			}
			else if (userChoice == 7)
			{
				showAllMenu();
			}
			else if (userChoice == 8)
			{
				countMenu();
			}
			else if (userChoice == 9)
			{
				displayMenu();
			}
			else
			{
				System.out.println("Input not valid please try again");
			}

			userChoice = getDataMenu();
		}
	}

	/*
	 * Add Method. Prompts user for input. Checks validation at each section.
	 * Gathers all inputs and creates a record in FileUtil
	 */
	public void addMenu() throws IOException
	{
		System.out.println("-------------------- Add Menu --------------------");
		System.out.println("\nAdd a new record to the database. This operation will not allow duplicates to be added to the database.");

		System.out.println("\nPlease add New Tv Show Details");
		System.out.println("\nPlease enter: ");

		String strPrompt = "\nTitle: ";
		String title = InputValidationUtility.getString(this.kb, strPrompt);

		strPrompt = "\nDescription: ";
		String desc = InputValidationUtility.getString(this.kb, strPrompt);

		strPrompt = "\nNumber of Seasons: ";
		int numOfSeasons = InputValidationUtility.getInt(this.kb, strPrompt);

		strPrompt = "\nLaunch Date: ";
		int launchDate = InputValidationUtility.getInt(this.kb, strPrompt);

		strPrompt = "\nAverage Run Time: ";
		double avgRunTime = InputValidationUtility
				.getDouble(this.kb, strPrompt);

		strPrompt = "\nRating: ";
		float rating = (float) InputValidationUtility.getDouble(this.kb,
				strPrompt);

		System.out
				.println("\nPlease enter 1 if Tv Show is shown on Netflix, Otherwise enter 0. ");
		strPrompt = "\nNetflix: ";
		int tru = 1;
		int min = 0;
		int max = 1;
		int x = InputValidationUtility.getInt(this.kb, strPrompt, min, max);

		boolean netflix = x == tru;

		/*
		 * Creating a new record.
		 */
		Record record = new Record(title, desc, numOfSeasons, launchDate,
				avgRunTime, rating, netflix);

		/*
		 * Inserting record into database.
		 */
		fileUtil.add(record);

	}

	/*
	 * Edit Method. Prompts user for input. Checks validation at each section.
	 * Gathers all inputs and edits a record in FileUtil
	 */
	public void editMenu() throws IOException
	{
		System.out.println("-------------------- Edit Menu --------------------");
		System.out.println("\Edit any of the fields of a given record in the database.");

				
		System.out
				.println("\nPlease add Tv Show Details you would like to edit");
		System.out.println("\nPlease enter: ");

		String strPrompt = "\nRecord You Would Like to Edit by Title: ";
		String edit = InputValidationUtility.getString(this.kb, strPrompt);

		strPrompt = "\nTitle: ";
		String title = InputValidationUtility.getString(this.kb, strPrompt);

		strPrompt = "\nDescription: ";
		String desc = InputValidationUtility.getString(this.kb, strPrompt);

		strPrompt = "\nNumber of Seasons: ";
		int numOfSeasons = InputValidationUtility.getInt(this.kb, strPrompt);

		strPrompt = "\nLaunch Date: ";
		int launchDate = InputValidationUtility.getInt(this.kb, strPrompt);

		strPrompt = "\nAverage Run Time: ";
		double avgRunTime = InputValidationUtility
				.getDouble(this.kb, strPrompt);

		strPrompt = "\nRating: ";
		float rating = (float) InputValidationUtility.getDouble(this.kb,
				strPrompt);

		System.out
				.println("\nPlease enter 1 if Tv Show is shown on Netflix, Otherwise enter 0. ");
		strPrompt = "\nNetflix: ";
		int tru = 1;
		int min = 0;
		int max = 1;
		int x = InputValidationUtility.getInt(this.kb, strPrompt, min, max);
		boolean netflix = x == tru;

		Record record = new Record(title, desc, numOfSeasons, launchDate,
				avgRunTime, rating, netflix);
		fileUtil.edit(edit, record);

	}

	/*
	 * Search by Regex Method. Prompts user for input. Checks validation at each
	 * section. Creates ArrayList of type record. Which accepts only matching
	 * records that match users regex. Prints the ArrayList.
	 */
	public void searchByRegexMenu() throws IOException
	{
		System.out.println("-------------------- Search by Regex Menu --------------------");
		System.out.println("\nDisplays the details of all records matching the regular expression specified.");
		System.out.println("\nPlease add Regex to search for a Tv Show Title ");

		String strPrompt = "\nPlease enter Regex: ";
		String regex = InputValidationUtility.getString(this.kb, strPrompt);

		ArrayList<Record> arr = fileUtil.searchByRegex(regex);
		if(arr != null)
		{
		for (int i = 0; i < arr.size(); i++)
		{
			System.out.println(arr.get(i));
		}
		}
	}

	/*
	 * Search by Range Method. Prompts user for two input. Checks validation at
	 * each section. Creates ArrayList of type record. Which accepts only
	 * matching records that match users inputed range. Prints the ArrayList.
	 */
	public void searchByRangeMenu() throws IOException
	{
		System.out.println("-------------------- Search by Range Menu --------------------");
		System.out.println("\nDisplays the details of all records corresponding to a user-specifed range.");
		System.out.println("\nPlease add Two Ranges to search for a Tv Show ");

		String strPrompt = "\nPlease enter Starting Year: ";
		int yearOne = InputValidationUtility.getInt(this.kb, strPrompt);

		strPrompt = "\nPlease enter Ending Year: ";
		int yearTwo = InputValidationUtility.getInt(this.kb, strPrompt);

		ArrayList<Record> arr = fileUtil.searchByRange(yearOne, yearTwo);
		if(arr != null)
		{for (int i = 0; i < arr.size(); i++)
		{
			System.out.println(arr.get(i));
		}
		}
	}

	/*
	 * Delete Record Method. Prompts user for input. Passes title into
	 * fileUtil.delete.
	 */
	public void deleteRecordMenu() throws IOException
	{
		System.out.println("-------------------- Delete a Single Record Menu --------------------");
		System.out.println("\nDeletes a user-specified record from the database.");
		String strPrompt = "\nPlease enter Record Title you would like to delete ";
		String title = InputValidationUtility.getString(this.kb, strPrompt);

		fileUtil.delete(title);
	}

	/*
	 * Delete file Method. Prompts user for input. Passes fileName into
	 * fileUtil.deleteAll.
	 */
	public void deleteAllMenu()
	{
		System.out.println("-------------------- Delete All Record Menu --------------------");
		System.out.println("\nDeletes all records from the database.");

		String strPrompt = "\nPlease enter File Name you would like to delete ";
		String fileName = InputValidationUtility.getString(this.kb, strPrompt);

		fileUtil.deleteAll(fileName);
	}

	/*
	 * Show All Method. Prints all records.
	 */
	public void showAllMenu() throws IOException
	{
		System.out.println("-------------------- Show All Record Menu --------------------");
		System.out.println("\nDisplay all records in the database.");
		fileUtil.showAll();
	}

	/*
	 * Count Method. Counts records.
	 */
	private void countMenu()
	{
		System.out.println("-------------------- Count All Record Menu --------------------");
		System.out.println("\nCounts all records in the database.");
		fileUtil.count();
	}

}
