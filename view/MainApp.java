package view;

/*
 * Imports
 */
import java.io.IOException;
import control.FileUtility;
import control.Menu;
import model.Record;

/**
 * @author Delwyn Monaghan and John McDonnell. Course: Computing Level 8. Group:
 *         2. Module: AOOD CA2. Due Date: 1st May 2015.
 */
public class MainApp
{

	public static void main(String[] args)
	{
		/*
		 * Instance of MainApp
		 */
		MainApp theApp = new MainApp();
		try
		{
			/*
			 * Starting Application
			 */
			theApp.start();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	/*
	 * loading data and initializing Menu
	 */
	private void start() throws IOException
	{
		//loadData();
		initialiseMenu();
	}

	/*
	 * Pre-loaded file containing data.
	 */
	private void loadData() throws IOException
	{
		Record r1 = new Record("Breaking Bad", "Drugs", 5, 2009, 43.2,
				(float) 9.6, true);
		Record r2 = new Record("Game of Thrones", "Violence", 4, 2010, 61.5,
				(float) 10.1, true);
		Record r3 = new Record("Better Call Saul", "Drugs", 5, 2007, 43.2,
				(float) 9.6, true);
		Record r4 = new Record("Suits", "Violence", 4, 2011, 61.5,
				(float) 10.1, true);
		Record r5 = new Record("Top Gear", "Drugs", 5, 2010, 43.2, (float) 9.6,
				true);
		Record r6 = new Record("Broadwalk Empire", "Violence", 4, 2014, 61.5,
				(float) 10.1, true);
		Record r7 = new Record("The Big Bang Theory", "Drugs", 5, 2012, 43.2,
				(float) 9.6, true);
		Record r8 = new Record("Family Guy", "Violence", 4, 2010, 61.5,
				(float) 10.1, true);

		/*
		 * Creating instance of FileUtility called system.
		 */
		FileUtility system = new FileUtility();

		/*
		 * Creating file.
		 */

		system.createFile("tv show database");
		/*
		 * Opening file
		 */
		system.openFile("tv show database");

		/*
		 * Adding records to file.
		 */
		system.add(r1);
		system.add(r2);
		system.add(r3);
		system.add(r4);
		system.add(r5);
		system.add(r6);
		system.add(r7);
		system.add(r8);

		/*
		 * Closing file.
		 */
		system.closeFile();

	}

	public void initialiseMenu() throws IOException
	{
		/*
		 * Creating an instance of menu.
		 */
		Menu menu = new Menu();
		menu.displayMenu();
	}

}
