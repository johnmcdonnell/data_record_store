package junit;

import java.io.File;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import control.FileUtility;


public class createTests extends TestCase{

	public createTests(String testName)
	{
		super(testName);
	}
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testCreate()
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tv");
		File f = new File("C:/temp/tv.txt");
		assertEquals(true, f.exists());
		
	}
	
	public void testCreate_NotFound()
	{
		FileUtility f1 = new FileUtility();
		f1.createFile(".tv");
		File f = new File("C:/temp/.tv.txt");
		assertEquals(false, f.exists());
		
	}
	public void testCreateUnique()
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tv5");
		assertEquals(false,f1.createFile("tv5"));
		
	}

}
