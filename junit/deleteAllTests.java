package junit;

import java.io.IOException;
import junit.framework.TestCase;
import model.Record;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import control.FileUtility;


public class deleteAllTests extends TestCase{
	
	public deleteAllTests(String testName)
	{
		super(testName);
	}
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDeleteAll() throws IOException
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tvDeleteAll");
		f1.openFile("tvDeleteAll");
		Record r1 = new Record("Breaking Bad", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		Record r2 = new Record("Game of Thrones", "Violence", 4, 2010, 61.5, (float) 10.1, true);
		
		f1.count();
		f1.add(r1);
		f1.add(r2);
		f1.count();
		f1.closeFile();
		f1.deleteAll("tvDeleteAll");
		f1.openFile("tvDeleteAll");
		f1.showAll();
		f1.count();
	}
}
