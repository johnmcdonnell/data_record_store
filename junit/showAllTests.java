package junit;

import java.io.IOException;
import junit.framework.TestCase;
import model.Record;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import control.FileUtility;


public class showAllTests extends TestCase{

	public showAllTests(String testName)
	{
		super(testName);
	}
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testShowAll() throws IOException
	{	
		FileUtility f1 = new FileUtility();
		f1.createFile("tvshowAll1");
		f1.openFile("tvshowAll1");
		Record r1 = new Record("Breaking Bad", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		Record r2 = new Record("Game of Thrones", "Violence", 4, 2010, 61.5, (float) 10.1, true);
		f1.add(r1);
		f1.add(r2);
		boolean pass = f1.showAll();
 		assertEquals(true, pass);
	}
	public void testShowAll_NoData() throws IOException
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tvshowAll2");
		f1.openFile("tvshowAll2");
		boolean pass = f1.showAll();
 		assertEquals(false, pass);
	}

}
