package junit;

import java.io.IOException;
import java.util.ArrayList;
import junit.framework.TestCase;
import model.Record;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import control.FileUtility;


public class regexTests extends TestCase{

	public regexTests(String testName)
	{
		super(testName);
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRegex() throws IOException
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tvRegex");
		f1.openFile("tvRegex");
		
		Record r1 = new Record("Breaking Bad", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		Record r2 = new Record("Brooklyn99", "Violence", 4, 2010, 61.5, (float) 10.1, true);
		Record r3 = new Record("Winnie the pooh", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		Record r4 = new Record("Lazy Town", "Violence", 4, 2010, 61.5, (float) 10.1, true);
		
		f1.add(r1);
		f1.add(r2);
		f1.add(r3);
		f1.add(r4);
		
		ArrayList<Record> arr = new ArrayList<Record>(); 
		arr = f1.searchByRegex("[a-zA-Z]{0,}[0-9]{0,}");
		assertNotNull(arr);	
	}
	
	public void testRegex_NotFound() throws IOException
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tvRegex2");
		f1.openFile("tvRegex2");
		
		Record r1 = new Record("Breaking Bad", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		Record r2 = new Record("Brooklyn99", "Violence", 4, 2010, 61.5, (float) 10.1, true);
		Record r3 = new Record("Winnie the pooh", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		Record r4 = new Record("Lazy Town", "Violence", 4, 2010, 61.5, (float) 10.1, true);
		
		f1.add(r1);
		f1.add(r2);
		f1.add(r3);
		f1.add(r4);
		
		ArrayList<Record> arr = new ArrayList<Record>(); 
		arr = f1.searchByRegex("[0-9]{1,}");
		assertNull(arr);
	}

}
