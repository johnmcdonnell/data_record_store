package junit;

import java.io.IOException;
import junit.framework.TestCase;
import model.Record;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import control.FileUtility;


public class editTests extends TestCase {
	public editTests(String testName)
	{
		super(testName);
	}

	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void testEdit() throws IOException
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tvEdit");
		f1.openFile("tvEdit");
		
		Record r1 = new Record("testEdit", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		Record r2 = new Record("testEdit2", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		f1.add(r1);
		
		f1.edit("testEdit",r2);
		Record r3 = f1.search("testEdit2");
		assertNotNull(r3);
	}
	public void testEdit_NotFound() throws IOException
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tvEdit2");
		f1.openFile("tvEdit2");
		
		Record r2 = new Record("testEdit2", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		
		boolean pass = f1.edit("testEdit",r2);;
		assertEquals(false, pass);
	}

}
