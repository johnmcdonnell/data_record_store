package junit;

import java.io.IOException;
import java.util.ArrayList;
import junit.framework.TestCase;
import model.Record;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import control.FileUtility;


public class rangeTests extends TestCase{

	public rangeTests(String testName)
	{
		super(testName);
	}
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRange() throws IOException
	{
		int count = 0;
		FileUtility f1 = new FileUtility();
		f1.createFile("tvRange");
		f1.openFile("tvRange");
		
		Record r1 = new Record("Breaking Bad", "Drugs", 5, 2011, 43.2, (float) 9.6, true);
		Record r2 = new Record("Brooklyn99", "Violence", 4, 2012, 61.5, (float) 10.1, true);
		Record r3 = new Record("Winnie the pooh", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		Record r4 = new Record("Lazy Town", "Violence", 4, 2013, 61.5, (float) 10.1, true);
		
		f1.add(r1);
		f1.add(r2);
		f1.add(r3);
		f1.add(r4);
		
		for(int i = 0;i<5;i++)
		{
		ArrayList<Record> arr = new ArrayList<Record>();
		arr = f1.searchByRange(2008,2010);
		System.out.println(arr.get(0).getTitle());
		if(arr.get(0).getTitle().equals("Winnie the pooh"))
		{
			count++;
		}
		}
		assertEquals(5, count);	
	}
	public void testRange_NotFound() throws IOException
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tvRange2");
		f1.openFile("tvRange2");
		
		Record r1 = new Record("Breaking Bad", "Drugs", 5, 2011, 43.2, (float) 9.6, true);
		Record r2 = new Record("Brooklyn99", "Violence", 4, 2012, 61.5, (float) 10.1, true);
		Record r3 = new Record("Winnie the pooh", "Drugs", 5, 2015, 43.2, (float) 9.6, true);
		Record r4 = new Record("Lazy Town", "Violence", 4, 2013, 61.5, (float) 10.1, true);
		
		f1.add(r1);
		f1.add(r2);
		f1.add(r3);
		f1.add(r4);
		
		ArrayList<Record> arr = new ArrayList<Record>();
		arr = f1.searchByRange(2008,2010);
		assertNull(arr);	
	}

}
