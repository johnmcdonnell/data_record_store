package junit;

import java.io.IOException;
import junit.framework.TestCase;
import model.Record;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import control.FileUtility;


public class deleteRecordsTest extends TestCase{

	public deleteRecordsTest(String testName)
	{
		super(testName);
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDelete() throws IOException
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tvDelete");
		f1.openFile("tvDelete");
		
		Record r1 = new Record("Breaking Bad", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		Record r2 = new Record("Game of Thrones", "Violence", 4, 2010, 61.5, (float) 10.1, true);
		Record r3 = new Record("Winnie the pooh", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		Record r4 = new Record("Lazy Town", "Violence", 4, 2010, 61.5, (float) 10.1, true);
		
		f1.add(r1);
		f1.add(r2);
		f1.add(r3);
		f1.add(r4);
		
		f1.delete("Winnie the pooh");
		Record r = f1.search("Winnie the pooh");
		assertNull(r);
	}
	public void testDelete_NotFound() throws IOException
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tvDelete2");
		f1.openFile("tvDelete2");
		
		Record r1 = new Record("Breaking Bad", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		Record r3 = new Record("Winnie the pooh", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		Record r4 = new Record("Lazy Town", "Violence", 4, 2010, 61.5, (float) 10.1, true);
		
		f1.add(r1);
		f1.add(r3);
		f1.add(r4);
		
		boolean pass = f1.delete("Game of Thrones");
		assertEquals(false,pass);
	}
}
