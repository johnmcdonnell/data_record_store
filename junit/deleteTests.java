package junit;

import java.io.File;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import control.FileUtility;


public class deleteTests extends TestCase{
	public deleteTests(String testName)
	{
		super(testName);
	}
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	@Test
	public void testDelete()
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tvDel");
		f1.deleteFile("tvDel");
		
		File f = new File("C:/temp/tvDel.txt");
		assertEquals(false, f.exists());
		
	}
	public void testDelete_NotFound()
	{

		FileUtility f1 = new FileUtility();
		boolean pass = f1.deleteFile("tvDel2");
		
		assertEquals(false, pass);
		
		
	}


}
