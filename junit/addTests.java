package junit;


import java.io.IOException;
import junit.framework.TestCase;
import model.Record;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import control.FileUtility;


public class addTests extends TestCase{
	
	public addTests(String testName)
	{
		super(testName);
	}
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAdd() throws IOException
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tvAdd");
		f1.openFile("tvAdd");
		Record r1 = new Record("testAdd", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		f1.add(r1);
		assertNotNull(f1.search("testAdd"));
		
	}
	public void testAddUnique() throws IOException
	{
		FileUtility f1 = new FileUtility();
		f1.createFile("tvAdd2");
		f1.openFile("tvAdd2");
		Record r1 = new Record("testAdd2", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		f1.add(r1);
		Record r2 = new Record("testAdd2", "Drugs", 5, 2009, 43.2, (float) 9.6, true);
		boolean pass = f1.add(r2);
		assertEquals(false,pass);
		
	}

}
